## Auctioneer 
Auctioneer for the bidder - auctioneer system.

    - handles bidders registered to it.
    - BidConductor conducts bid rounds with a deadline of 200ms
    

### Limitations / Some things which could be improved
    - Does not keep track of when bidders go offline
    - Network latency is not taken care of within the 200ms deadline

### Endpoints
 - `/bidder/register`
 - `/bidder/list`
 - `/ad/request`