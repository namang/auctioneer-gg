package main

import (
	"gitlab.com/namang/auctioneer-gg/handlers"
	"log"
	"net/http"
	"sync"
)

func main() {
	bidConductor := handlers.BidConductor{Mux: &sync.Mutex{}}
	http.HandleFunc("/bidder/register", bidConductor.RegisterBidder)
	http.HandleFunc("/ad/request", bidConductor.BidRoundHandler)
	http.HandleFunc("/bidder/list", bidConductor.ListBidders)

	err := http.ListenAndServe(":8090", nil)
	if err != nil {
		log.Fatal("Error starting server", err.Error())
	}
}
