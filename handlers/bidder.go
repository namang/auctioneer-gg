package handlers

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
)

func (b *Bidder) RaiseBidRequest(client *http.Client, request AdRequest) chan AdResponse {
	requestContent, err := json.Marshal(request)
	if err != nil {
		log.Println("There was an error marshalling request data", err.Error())
		return nil
	}
	req, _ := http.NewRequest("POST", b.BidderUrl, bytes.NewBuffer(requestContent))
	adResponseChannel := make(chan AdResponse, 1)
	go func() {
		response, err := client.Do(req)
		if err != nil {
			log.Println("Request not complete", err.Error())
			close(adResponseChannel)
		} else {
			defer response.Body.Close()

			var bidResponse AdResponse
			err = json.NewDecoder(response.Body).Decode(&bidResponse)
			log.Println(bidResponse)
			adResponseChannel <- bidResponse
			close(adResponseChannel)
		}

	}()

	return adResponseChannel
}
