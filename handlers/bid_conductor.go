package handlers

import (
	"encoding/json"
	"github.com/google/uuid"
	"log"
	"math"
	"net/http"
	"time"
)

func (b *BidConductor) BidRoundHandler(res http.ResponseWriter, req *http.Request) {
	var auctionRequest AuctionRequest
	err := json.NewDecoder(req.Body).Decode(&auctionRequest)
	if err != nil {
		res.WriteHeader(500)
		_, _ = res.Write([]byte("Internal Service Error"))
		return
	}

	auctionResponse := b.StartBidding(bidders, NewAdRequest(auctionRequest))
	response, err := json.Marshal(auctionResponse)
	if err != nil {
		log.Println("Error marshalling data", err.Error())
		res.WriteHeader(500)
		_, _ = res.Write([]byte("Internal Service Error"))
		return
	}

	_, _ = res.Write(response)
}

func (b *BidConductor) RegisterBidder(res http.ResponseWriter, req *http.Request) {
	b.Mux.Lock()
	defer b.Mux.Unlock()

	var bidderRequest Bidder
	err := json.NewDecoder(req.Body).Decode(&bidderRequest)
	if err != nil {
		log.Println("Unable to register bidder", err.Error())
		res.WriteHeader(500)
		_, _ = res.Write([]byte("Internal Service Error"))
		return
	}

	bidderRequest.BidderId = uuid.New().String()

	bidders = append(bidders, &bidderRequest)

	bidderResponse, err := json.Marshal(bidderRequest)
	if err != nil {
		log.Println("Error marshalling bidder request", err.Error())
		res.WriteHeader(500)
		_, _ = res.Write([]byte("Internal Service Error"))
		return
	}
	_, _ = res.Write(bidderResponse)
}

func (b *BidConductor) StartBidding(bidders []*Bidder, request AdRequest) AuctionResponse {
	b.Mux.Lock()
	defer b.Mux.Unlock()

	client := http.Client{
		Timeout: 200 * time.Millisecond,
	}

	var bidChannels []chan AdResponse
	for _, bidder := range bidders {
		bidChannels = append(bidChannels, bidder.RaiseBidRequest(&client, request))
	}

	timer := time.NewTimer(1000 * time.Millisecond)
	<-timer.C

	maxBid := math.Inf(-1)

	var winnerBidder string
	for _, bidChan := range bidChannels {
		if bidChan != nil {
			bidResponse, ok := <-bidChan
			if !ok {
				log.Println("no value")
				continue
			}
			log.Println("in channel", bidResponse)
			if bidResponse.BidValue > maxBid {
				maxBid = bidResponse.BidValue
				winnerBidder = bidResponse.BidderId
			}
		}
	}

	auctionResponse := AuctionResponse{
		AdResponse: AdResponse{
			BidValue: maxBid,
			BidderId: winnerBidder,
		},
	}

	return auctionResponse
}

func (b *BidConductor) ListBidders(res http.ResponseWriter, req *http.Request) {
	b.Mux.Lock()
	defer b.Mux.Unlock()

	var bidderUrls []string
	for _, bidder := range bidders {
		bidderUrls = append(bidderUrls, bidder.BidderUrl)
	}

	response, err := json.Marshal(bidderUrls)
	if err != nil {
		log.Println("Error marshalling bidder urls", err.Error())
		res.WriteHeader(500)
		_, _ = res.Write([]byte("Internal Service Error"))
	}

	_, _ = res.Write(response)
}
