package handlers

import "sync"

type Bidder struct {
	BidderUrl string `json:"bidder_url"`
	BidderId  string `json:"bidder_id"`
}

type BidConductor struct {
	Mux *sync.Mutex
}

type AdRequest struct {
	AuctionId string `json:"auction_id"`
}

type AdResponse struct {
	BidValue float64 `json:"bid_value"`
	BidderId string  `json:"bidder_id"`
}

type AuctionRequest struct {
	AuctionId string `json:"auction_id"`
}

type AuctionResponse struct {
	AdResponse
}

var bidders []*Bidder
